﻿namespace StreamsLab.Models
{
    public class Employee : Person
    {
        public Employee()
        {
            IsAdmin = false;
        }
    }
}
