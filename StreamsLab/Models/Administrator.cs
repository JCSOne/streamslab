﻿namespace StreamsLab.Models
{
    public class Administrator : Person
    {
        public int Commission { get; set; }

        public Administrator()
        {
            IsAdmin = true;
        }
    }
}
