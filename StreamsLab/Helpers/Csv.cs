﻿using StreamsLab.Models;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace StreamsLab.Helpers
{
    public static class Csv
    {
        public static void Save(IEnumerable<Person> people)
        {
            var dlg = new Microsoft.Win32.SaveFileDialog
            {
                FileName = "Personas",
                DefaultExt = ".csv",
                Filter = "Text documents (.csv)|*.csv"
            };

            var result = dlg.ShowDialog();

            var fileName = result == true ? dlg.FileName : string.Empty;

            if (string.IsNullOrWhiteSpace(fileName))
            {
                return;
            }

            using (var sw = new StreamWriter(new FileStream(fileName, FileMode.OpenOrCreate), Encoding.UTF8))
            {
                var sb = new StringBuilder();
                sb.AppendLine("Documento,Nombre,Apellido,Teléfono,Correo,Salario,¿Administrador?,Comisiones");
                foreach (var person in people)
                {
                    sb.AppendLine($"{person.Id},{person.Name},{person.Surname},{person.PhoneNumber},{person.Email},{person.Salary},{person.IsAdmin}{(person.IsAdmin ? $",{((Administrator)person).Commission}" : string.Empty)}");
                }
                sw.Write(sb.ToString());
            }
        }

        public static IEnumerable<Person> Read()
        {
            var dlg = new Microsoft.Win32.OpenFileDialog
            {
                InitialDirectory = "C:\\",
                Filter = "Text documents (.csv)|*.csv"
            };

            var result = dlg.ShowDialog();

            var fileName = result == true ? dlg.FileName : string.Empty;

            if (string.IsNullOrWhiteSpace(fileName))
            {
                return null;
            }

            var people = new List<Person>();

            using (var sr = new StreamReader(new FileStream(fileName, FileMode.Open), Encoding.UTF8))
            {
                while (!sr.EndOfStream)
                {
                    var line = sr.ReadLine();
                    if (line == null)
                    {
                        continue;
                    }

                    var values = line.Split(',');
                    if (values[0].ToLower().Equals("documento"))
                    {
                        continue;
                    }

                    int.TryParse(values[0], out var id);
                    int.TryParse(values[5], out var salary);
                    bool.TryParse(values[6], out var isAdmin);
                    if (isAdmin)
                    {
                        int.TryParse(values[7], out var commission);
                        people.Add(new Administrator()
                        {
                            Id = id,
                            Name = values[1],
                            Surname = values[2],
                            Email = values[3],
                            Salary = salary,
                            PhoneNumber = values[5],
                            Commission = commission
                        });
                        continue;
                    }
                    people.Add(new Employee()
                    {
                        Id = id,
                        Name = values[1],
                        Surname = values[2],
                        Email = values[3],
                        Salary = salary,
                        PhoneNumber = values[5]
                    });
                }
            }

            return people;
        }
    }
}
