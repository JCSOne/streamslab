﻿using StreamsLab.Helpers;
using StreamsLab.Models;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace StreamsLab.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        private ObservableCollection<Person> _people;
        public ObservableCollection<Person> People => _people ?? (_people = new ObservableCollection<Person>());

        private Administrator _selectedPerson;
        public Administrator SelectedPerson
        {
            get => _selectedPerson ?? (_selectedPerson = new Administrator());
            set { _selectedPerson = value; OnPropertyChanged("SelectedPerson"); }
        }

        private ICommand _addUserCommand;
        public ICommand AddUserCommand => _addUserCommand ?? (_addUserCommand = new DelegateCommand(AddUser));

        private ICommand _exportUsersCommand;
        public ICommand ExportUsersCommand => _exportUsersCommand ?? (_exportUsersCommand = new DelegateCommand(CreateFile));

        private ICommand _importUsersCommand;
        public ICommand ImportUsersCommand => _importUsersCommand ?? (_importUsersCommand = new DelegateCommand(ReadFile));

        private void AddUser(object parameter)
        {
            if (SelectedPerson.IsAdmin)
            {
                People.Add(SelectedPerson);
                SelectedPerson = new Administrator();
                return;
            }
            People.Add(new Employee()
            {
                Name = SelectedPerson.Name,
                Id = SelectedPerson.Id,
                Surname = SelectedPerson.Surname,
                Email = SelectedPerson.Email,
                PhoneNumber = SelectedPerson.PhoneNumber,
                Salary = SelectedPerson.Salary
            });
            SelectedPerson = new Administrator();
        }

        private void CreateFile(object parameter)
        {
            Csv.Save(People.ToList());
        }

        private void ReadFile(object parameter)
        {
            People.Clear();
            foreach (var person in Csv.Read())
            {
                People.Add(person);
            }
        }
    }
}
