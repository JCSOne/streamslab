﻿using System.ComponentModel;

namespace StreamsLab.ViewModels
{
    public class ViewModelBase : INotifyPropertyChanged
    {
        public void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
